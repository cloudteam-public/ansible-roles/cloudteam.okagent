Ansible Role: Okagent for postgres
=========

Configure okagent for postgres

Requirements
------------

None.


Role Variables
--------------
Available variables are listed below, along with default values (see `defaults/main.yml`):

Variable | Default | Description
---------|----------|---------
 `ok_pg_version` | 13 | Version of postgres installed on host
 `ok_token` | ch4n-g3m3-p13a-se1a-mp1ac3h0ld33 | Token for okmeter
 `ok_password` | SooP3r_S3kReT | Password for user `okagent`
 `ok_userid` | n0n-n0n3 | UserID for okmeter
 `ok_pg_port` | 5432 | Postgres port`

 
Dependencies
------------

None.

Example Playbook
----------------
```yml
    - hosts: databases
      roles:
         - role: cloudteam.okagent
```


License
-------

BSD 

Author Information
------------------

This role was created in 2021 by [Vlad Bubnov](https://github.com/Faust13).
